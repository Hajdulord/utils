package com.hmf.complex;

import java.util.Objects;

/***
 * Data structure for Complex numbers
 * @author Hajdu Marcell Ferenc
 * @version 1.0
 */


public class Complex {

    /***
     * @param imaginary the imaginary part of a complex number
     * @param real the real part of a complex number
     */
    private double imaginary, real;

    /***
     * The constructor of a Complex number
     * @param imaginary sets the imaginary part
     * @param real sets the real part
     * @since 1.0
     */
    public Complex(double imaginary, double real) {
        this.imaginary = imaginary;
        this.real = real;
    }

    /***
     * Constructor for a Complex number
     * sets the imaginary and the real values to 0
     * @since 1.0
     */
    public Complex() {
        imaginary = 0;
        real = 0;
    }

    /***
     * getter of the imaginary part
     * @return double value of the imaginary part
     * @since 1.0
     */
    public double getImaginary() {
        return imaginary;
    }

    /***
     * getter of the real part
     * @return double value of the real part
     * @since 1.0
     */
    public double getReal() {
        return real;
    }

    /***
     * setter of the imaginary part
     * @param imaginary sets this value as the imaginary part
     * @since  1.0
     */
    public void setImaginary(double imaginary) {
        this.imaginary = imaginary;
    }

    /***
     * setter ot the real part
     * @param real sets this value as the real part
     * @since 1.0
     */
    public void setReal(double real) {
        this.real = real;
    }

    /***
     * Default generated equals
     * @param o checks if o is equal to this
     * @return true if equals false if not
     * @since 1.0
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Complex)) return false;
        Complex complex = (Complex) o;
        return Double.compare(complex.getImaginary(), getImaginary()) == 0 &&
                Double.compare(complex.getReal(), getReal()) == 0;
    }

    /***
     * Default generated hashCode
     * @return int value generated from the imaginary and the real part with Object.hash
     * @since 1.0
     */
    @Override
    public int hashCode() {
        return Objects.hash(getImaginary(), getReal());
    }

    /***
     * Creates string of a Complex number like: 1 + 2i
     * @return string form of a Complex number
     * @since 1.0
     */
    @Override
    public String toString() {
        String temp = " +";
        if(imaginary < 0) temp = " ";
        return real + temp + imaginary + "i";
    }

    /***
     * The summation for two Complex numbers
     * @param other adds this Complex number to this
     * @return the new Complex number created as the summation of the other and this
     * @since 1.0
     */
    public Complex add(Complex other){
        return new Complex(this.imaginary + other.imaginary, this.real + other.real);
    }

    /***
     * The substraction of the other Complex number from this
     * @param other will be substracted from this
     * @return the new Complex number created as the substraction of other from this
     */
    public  Complex substract(Complex other){
        return new Complex(this.imaginary - other.imaginary, this.real - other.real);
    }
}
