package com.hmf.complex;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/***
 * The test files for the Complex numbers
 * @author Hajdu Marcell Ferenc
 */
class ComplexTest {

    /***
     * Test of the imaginary getter
     */
    @Test
    void testGetImaginary() {
        Complex num1 = new Complex();
        Complex num2 = new Complex(1, 1);
        assertEquals(0, num1.getImaginary());
        assertEquals(1, num2.getImaginary());
    }

    /***
     * Test of the real getter
     */
    @Test
    void testGetReal() {
        Complex num1 = new Complex();
        Complex num2 = new Complex(1, 1);
        assertEquals(1, num2.getReal());
    }

    /***
     * Test of the imaginary setter
     */
    @Test
    void testSetImaginary() {
        Complex num1 = new Complex();
        num1.setImaginary(3);
        assertEquals(3, num1.getImaginary());
    }

    /***
     * Test of the real setter
     */
    @Test
    void testSetReal() {
        Complex num1 = new Complex();
        num1.setReal(3);
        assertEquals(3, num1.getReal());
    }

    /***
     * Test of the equals method
     */
    @Test
    void testEquals() {
        Complex num1 = new Complex();
        Complex num2 = new Complex();
        Complex num3 = new Complex(1, 1);

        assertTrue(num1.equals(num2));
        assertFalse(num1.equals(num3));
    }

    /***
     * Test of the toString method
     */
    @Test
    void testToString() {
        Complex num1 = new Complex();
        Complex num2 = new Complex(-1, 1);
        Complex num3 = new Complex(1,1);

        assertEquals("0.0 +0.0i", num1.toString());
        assertEquals("1.0 -1.0i", num2.toString());
        assertEquals("1.0 +1.0i", num3.toString());

    }

    /***
     * test of add
     */
    @Test
    void testAdd() {
        Complex num1 = new Complex();
        Complex num2 = new Complex(-1, 1);
        Complex num3 = new Complex(1,1);

        Complex numTest = new Complex(-1, 1);
        assertTrue(numTest.equals(num1.add(num2)));

        numTest = new Complex(0, 2);
        assertTrue(numTest.equals(num2.add(num3)));
    }

    /***
     * test of substract
     */
    @Test
    void testSubstract() {
        Complex num1 = new Complex();
        Complex num2 = new Complex(-1, 1);
        Complex num3 = new Complex(1,1);

        Complex numTest = new Complex(1, -1);
        assertTrue(numTest.equals(num1.substract(num2)));

        numTest = new Complex(-2, 0);
        assertTrue(numTest.equals(num2.substract(num3)));
    }
}